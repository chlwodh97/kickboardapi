### 위경도 API 세팅 가이드

###### postgreSQL extension
```
create extension cube;
create extension earthdistance;
```
* 먼저 postgreSQL에 위 Extension 설치 할 것.

###### 위경도 알려주는 사이트
```
http://map.esran.com/
```

###### 위경도 QUERY
```
현재 위치를 기준으로 반경 n미터 안의 킥보드 찾기
select * from kick_board p where earth_distance(ll_to_earth(37.63370850186704, 127.04118162032388), ll_to_earth(p.latitude, p.longitude)) < 5000;
```

###### flutter gps package
```
https://pub.dev/packages/geolocator
```

###### web gps
```
web은 정확한 gps 위치정보 가져오기가 불가능함. IP기반으로 위치 정보를 가져오게됨.
```

###### procedure
```
CREATE OR REPLACE FUNCTION public.get_near_kickboard(positionX DOUBLE PRECISION, positionY DOUBLE PRECISION, distance DOUBLE PRECISION)
RETURNS TABLE
(
    kick_board_status varchar
    , model_name varchar
    , price_basis varchar
    , posx double precision
    , posy double precision
    , distance_m double precision
)
as
$$
DECLARE
    v_record RECORD;
BEGIN
    for v_record in (
        select
            kick_board.kick_board_status,
            kick_board.model_name,
            kick_board.price_basis,
            kick_board.posx,
            kick_board.posy,
            earth_distance(ll_to_earth(kick_board.posx, kick_board.posy), ll_to_earth(positionX, positionY)) as distance_m
        from kick_board
        where earth_distance(ll_to_earth(kick_board.posx, kick_board.posy), ll_to_earth(positionX, positionY)) <= distance
        order by distance_m
    )
    loop
        kick_board_status := v_record.kick_board_status;
        model_name := v_record.model_name;
        price_basis := v_record.price_basis;
        posx := v_record.posx;
        posy := v_record.posy;
        distance_m := v_record.distance_m;
        return next;
    end loop;
END;
$$
LANGUAGE plpgsql
```