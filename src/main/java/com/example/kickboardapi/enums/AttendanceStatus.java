package com.example.kickboardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AttendanceStatus {
    START("출석")
    , END("퇴실");

    private final String name;
}
