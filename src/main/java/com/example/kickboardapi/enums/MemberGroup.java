package com.example.kickboardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberGroup {
    ROLE_ADMIN("최고관리자")
    , ROLE_USER("사용자")
    , ROLE_OFFICE("사무")
    , ROLE_SALES("영업")
    , ROLE_PICKUP("수거")
    ;

    private final String name;
}
