package com.example.kickboardapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PriceBasis {
    BASIC("베이직", 1000D, 100D),
    PRIMEUM("프리미엄", 2000D, 200D);

    private final String name;
    private final Double basePrice;
    private final Double perMinPrice;
}
