package com.example.kickboardapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
