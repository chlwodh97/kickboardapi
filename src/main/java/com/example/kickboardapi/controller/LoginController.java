package com.example.kickboardapi.controller;

import com.example.kickboardapi.enums.MemberGroup;
import com.example.kickboardapi.model.common.SingleResult;
import com.example.kickboardapi.model.member.LoginRequest;
import com.example.kickboardapi.model.member.LoginResponse;
import com.example.kickboardapi.service.member.LoginService;
import com.example.kickboardapi.service.common.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Api(tags = "로그인")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member/login")
public class LoginController {
    private final LoginService loginService;

    @ApiOperation(value = "웹 - 관리자 로그인")
    @PostMapping("/web/admin")
    public SingleResult<LoginResponse> doLoginAdmin(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_ADMIN, loginRequest, "WEB"));
    }

    @ApiOperation(value = "앱 - 일반유저 로그인")
    @PostMapping("/app/user")
    public SingleResult<LoginResponse> doLoginUser(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_USER, loginRequest, "APP"));
    }

    @ApiOperation(value = "앱 - 사무 직원 로그인")
    @PostMapping("/app/office")
    public SingleResult<LoginResponse> doLoginOffice(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_OFFICE, loginRequest, "APP"));
    }

    @ApiOperation(value = "앱 - 영업 직원 로그인")
    @PostMapping("/app/sales")
    public SingleResult<LoginResponse> doLoginSales(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_SALES, loginRequest, "APP"));
    }

    @ApiOperation(value = "앱 - 수거 직원 로그인")
    @PostMapping("/app/pickup")
    public SingleResult<LoginResponse> doLoginPickup(@RequestBody @Valid LoginRequest loginRequest) {
        return ResponseService.getSingleResult(loginService.doLogin(MemberGroup.ROLE_PICKUP, loginRequest, "APP"));
    }
}
