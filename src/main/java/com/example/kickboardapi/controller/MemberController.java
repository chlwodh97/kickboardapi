package com.example.kickboardapi.controller;

import com.example.kickboardapi.entity.Member;
import com.example.kickboardapi.model.common.CommonResult;
import com.example.kickboardapi.model.member.FindPasswordRequest;
import com.example.kickboardapi.model.member.MemberCreateRequest;
import com.example.kickboardapi.model.member.MemberJoinRequest;
import com.example.kickboardapi.model.member.PasswordChangeRequest;
import com.example.kickboardapi.service.member.MemberDataService;
import com.example.kickboardapi.service.common.ResponseService;
import com.example.kickboardapi.service.member.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final ProfileService profileService;
    private final MemberDataService memberDataService;

    @ApiOperation(value = "회원 등록") // 관리자용..
    @PostMapping("/data")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest memberJoinRequest) {
        memberDataService.setMember(memberJoinRequest.getMemberGroup(), memberJoinRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 가입")
    @PostMapping("/join")
    public CommonResult joinMember(@RequestBody @Valid MemberCreateRequest memberCreateRequest) {
        memberDataService.setMember(memberCreateRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "비밀번호 수정")
    @PutMapping("/password")
    public CommonResult putPassword(@RequestBody @Valid PasswordChangeRequest changeRequest) {
        Member member = profileService.getMemberData();
        memberDataService.putPassword(member, changeRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "비밀번호 찾기")
    @PutMapping("/find-password")
    public CommonResult findPassword(@RequestBody @Valid FindPasswordRequest request) {
        memberDataService.putPassword(request);

        return ResponseService.getSuccessResult();
    }
}
