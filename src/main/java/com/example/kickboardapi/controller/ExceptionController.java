package com.example.kickboardapi.controller;

import com.example.kickboardapi.exception.CAccessDeniedException;
import com.example.kickboardapi.exception.CAuthenticationEntryPointException;
import com.example.kickboardapi.model.common.CommonResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exception")
public class ExceptionController {
    @GetMapping("/access-denied")
    public CommonResult accessDeniedException() {
        throw new CAccessDeniedException();
    }

    @GetMapping("/entry-point")
    public CommonResult entryPointException() {
        throw new CAuthenticationEntryPointException();
    }
}
