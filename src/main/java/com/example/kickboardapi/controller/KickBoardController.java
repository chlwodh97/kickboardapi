package com.example.kickboardapi.controller;


import com.example.kickboardapi.enums.KickBoardStatus;
import com.example.kickboardapi.enums.PriceBasis;
import com.example.kickboardapi.model.common.CommonResult;
import com.example.kickboardapi.model.common.ListResult;
import com.example.kickboardapi.model.kickboard.KickBoardItem;
import com.example.kickboardapi.model.kickboard.KickBoardSearchRequest;
import com.example.kickboardapi.service.common.ResponseService;
import com.example.kickboardapi.service.kickBoard.KickBoardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Api(tags = "킥보드 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/kick-board")
public class KickBoardController {
    private final KickBoardService kickBoardService;

    @PostMapping("/file-upload")
    public CommonResult setKickBoards(@RequestParam("csvFile") MultipartFile csvFile) throws Exception {
        kickBoardService.setKickBoards(csvFile);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "근처 킥보드 리스트")
    @GetMapping("/near")
    public ListResult<KickBoardItem> getNearList(@RequestParam("posX") double posX, @RequestParam("posY") double posY, @RequestParam("distanceKm") int distanceKm) {
        return ResponseService.getListResult(kickBoardService.getNearList(posX, posY, distanceKm), true);
    }

    @ApiOperation(value = "킥보드 리스트(관리자용)")
    @GetMapping("/all/{pageNum}")
    public ListResult<KickBoardItem> getList(
            @PathVariable int pageNum,
            @RequestParam(value = "kickBoardStatus", required = false) KickBoardStatus kickBoardStatus,
            @RequestParam(value = "priceBasis", required = false) PriceBasis priceBasis,
            @RequestParam(value = "modelName", required = false) String modelName,
            @RequestParam(value = "isUse", required = false) String isUse
    ) {
        KickBoardSearchRequest searchRequest = new KickBoardSearchRequest();
        searchRequest.setKickBoardStatus(kickBoardStatus);
        searchRequest.setPriceBasis(priceBasis);
        searchRequest.setModelName(modelName);
        if (isUse != null) searchRequest.setIsUse(isUse.equals("Y"));

        return ResponseService.getListResult(kickBoardService.getKickboards(pageNum, searchRequest), true);
    }
}
