package com.example.kickboardapi.controller;

import com.example.kickboardapi.model.common.CommonResult;
import com.example.kickboardapi.model.member.AuthPhoneCompleteRequest;
import com.example.kickboardapi.model.member.AuthPhoneSendRequest;
import com.example.kickboardapi.service.common.ResponseService;
import com.example.kickboardapi.service.member.AuthPhoneService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "핸드폰 인증 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/auth-phone")
public class AuthPhoneController {
    private final AuthPhoneService authPhoneService;

    @PostMapping("/auth/start")
    public CommonResult setAuthNumber(@RequestBody @Valid AuthPhoneSendRequest request) {
        authPhoneService.setAuthNumber(request);

        return ResponseService.getSuccessResult();
    }

    @PutMapping("/auth/end")
    public CommonResult putAuthStatus(@RequestBody @Valid AuthPhoneCompleteRequest request) {
        authPhoneService.putAuthStatus(request);

        return ResponseService.getSuccessResult();
    }
}
