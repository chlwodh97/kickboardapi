package com.example.kickboardapi.model.member;

import com.example.kickboardapi.enums.MemberGroup;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class MemberJoinRequest extends MemberCreateRequest {
    @NotNull
    @Enumerated(EnumType.STRING)
    private MemberGroup memberGroup;
}
