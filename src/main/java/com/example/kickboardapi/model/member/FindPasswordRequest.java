package com.example.kickboardapi.model.member;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class FindPasswordRequest {
    @ApiModelProperty(notes = "아이디", required = true)
    @NotNull
    @Length(min = 5, max = 30)
    private String username;

    @ApiModelProperty(notes = "연락처", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;
}
