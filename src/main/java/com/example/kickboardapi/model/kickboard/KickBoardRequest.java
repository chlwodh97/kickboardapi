package com.example.kickboardapi.model.kickboard;

import com.example.kickboardapi.enums.PriceBasis;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class KickBoardRequest {
    @NotNull
    @Length(min = 3, max = 20)
    private String modelName;

    @NotNull
    @Enumerated(value = EnumType.STRING)
    private PriceBasis priceBasis;

    @NotNull
    private LocalDate dateBuy;

    @NotNull
    private Double posX;

    @NotNull
    private Double posY;
}
