package com.example.kickboardapi.model.kickboard;

import com.example.kickboardapi.enums.KickBoardStatus;
import com.example.kickboardapi.enums.PriceBasis;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
public class KickBoardSearchRequest {
    // 킥보드 상태
    @Enumerated(value = EnumType.STRING)
    private KickBoardStatus kickBoardStatus;

    // 모델명
    private String modelName;

    // 기준요금
    @Enumerated(value = EnumType.STRING)
    private PriceBasis priceBasis;

    // 사용여부
    private Boolean isUse;
}
