package com.example.kickboardapi.repository;

import com.example.kickboardapi.entity.AuthPhone;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthPhoneRepository extends JpaRepository<AuthPhone, Long> {
    Optional<AuthPhone> findByPhoneNumber(String phoneNumber);
}
