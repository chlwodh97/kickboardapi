package com.example.kickboardapi.repository;

import com.example.kickboardapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findByUsername(String username);
    long countByUsername(String username);
//    Optional<Member> findByUsernameAndPhoneNumber(String username, String phoneNumber);
}
