package com.example.kickboardapi.repository;

import com.example.kickboardapi.entity.MessageSendHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageSendHistoryRepository extends JpaRepository<MessageSendHistory, Long> {
}
