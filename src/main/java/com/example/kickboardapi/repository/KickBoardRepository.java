package com.example.kickboardapi.repository;

import com.example.kickboardapi.entity.KickBoard;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface KickBoardRepository extends JpaRepository<KickBoard, Long> {
}
