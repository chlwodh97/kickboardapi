package com.example.kickboardapi.entity;

import com.example.kickboardapi.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AuthPhone {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    @Column(nullable = false, length = 6)
    private String authNumber;

    @Column(nullable = false)
    private Boolean isAuthComplete;

    public void putComplete() {
        this.isAuthComplete = true;
    }

    private AuthPhone(Builder builder) {
        this.phoneNumber = builder.phoneNumber;
        this.authNumber = builder.authNumber;
        this.isAuthComplete = builder.isAuthComplete;
    }

    public static class Builder implements CommonModelBuilder<AuthPhone> {
        private final String phoneNumber;
        private final String authNumber;
        private final Boolean isAuthComplete;

        public Builder(String phoneNumber, String authNumber) {
            this.phoneNumber = phoneNumber;
            this.authNumber = authNumber;
            this.isAuthComplete = false;
        }

        @Override
        public AuthPhone build() {
            return new AuthPhone(this);
        }
    }
}
