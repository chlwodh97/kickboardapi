package com.example.kickboardapi.service.message;

import com.example.kickboardapi.entity.MessageSendHistory;
import com.example.kickboardapi.enums.MessageTemplate;
import com.example.kickboardapi.repository.MessageSendHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;

// 용도 : 카톡, 문자 등 메세지 발송해주는 팀.. 원래는 알리고 api 여기서 호출하고 보낸 발송 내역 저장도 함.
@Service
@RequiredArgsConstructor
public class MessageService {
    private final MessageSendHistoryRepository messageSendHistoryRepository;

    public void sendMessage(
            MessageTemplate messageTemplate,
            String sendPhoneNumber,
            String receiverPhoneNumber,
            HashMap<String, String> contents
    ) {
        // 알리고 api 호출하기
        // MessageSendHistory에 데이터 저장하기
        String messageContentOrigin = messageTemplate.getContents();
        for (String key : contents.keySet()) {
            String keyResult = "#\\{" + key + "}";
            messageContentOrigin = messageContentOrigin.replaceAll(keyResult, contents.get(key));
        }

        MessageSendHistory messageSendHistory = new MessageSendHistory.Builder(sendPhoneNumber, receiverPhoneNumber, messageContentOrigin).build();
        messageSendHistoryRepository.save(messageSendHistory);
    }
}
