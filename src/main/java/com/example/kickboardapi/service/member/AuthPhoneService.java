package com.example.kickboardapi.service.member;

import com.example.kickboardapi.entity.AuthPhone;
import com.example.kickboardapi.enums.MessageTemplate;
import com.example.kickboardapi.exception.CMissingDataException;
import com.example.kickboardapi.model.member.AuthPhoneCompleteRequest;
import com.example.kickboardapi.model.member.AuthPhoneSendRequest;
import com.example.kickboardapi.repository.AuthPhoneRepository;
import com.example.kickboardapi.service.message.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AuthPhoneService {
    private final AuthPhoneRepository authPhoneRepository;
    private final MessageService messageService;

    public void setAuthNumber(AuthPhoneSendRequest request) {
        Optional<AuthPhone> authPhoneCheckData = authPhoneRepository.findByPhoneNumber(request.getPhoneNumber());

        if (authPhoneCheckData.isPresent()) throw new CMissingDataException();

        String resultRandomNumber = makeRandomNumber();

        AuthPhone authPhone = new AuthPhone.Builder(request.getPhoneNumber(), resultRandomNumber).build();
        authPhoneRepository.save(authPhone);

        HashMap<String, String> msgMap = new HashMap<>();
        msgMap.put("인증번호", resultRandomNumber);

        messageService.sendMessage(
                MessageTemplate.TD_8763,
                "1577-5555",
                request.getPhoneNumber(),
                msgMap
        );
    }

    public void putAuthStatus(AuthPhoneCompleteRequest request) {
        Optional<AuthPhone> authPhone = authPhoneRepository.findByPhoneNumber(request.getPhoneNumber());

        // 만약.... 데이터가 없다면... 중간에 해커가 데이터 변조한거임..그러니까 던져야함..
        if (authPhone.isEmpty()) throw new CMissingDataException();

        // 만약.. 인증번호가 일치하지 않는다면.... 상태를 바꾸지 못함... 던져야함..
        if (!authPhone.get().getAuthNumber().equals(request.getAuthNumber())) throw new CMissingDataException();

        AuthPhone authPhoneOrigin = authPhone.get();
        authPhoneOrigin.putComplete();
        authPhoneRepository.save(authPhoneOrigin);
    }

    private String makeRandomNumber() {
        // 예린님 로직 코드 참고해서 넣기..
        return "123456";
    }
}
