package com.example.kickboardapi.service.member;

import com.example.kickboardapi.entity.AuthPhone;
import com.example.kickboardapi.entity.Member;
import com.example.kickboardapi.enums.MemberGroup;
import com.example.kickboardapi.enums.MessageTemplate;
import com.example.kickboardapi.exception.CMissingDataException;
import com.example.kickboardapi.exception.CWrongPhoneNumberException;
import com.example.kickboardapi.lib.CommonCheck;
import com.example.kickboardapi.model.member.FindPasswordRequest;
import com.example.kickboardapi.model.member.MemberCreateRequest;
import com.example.kickboardapi.model.member.PasswordChangeRequest;
import com.example.kickboardapi.repository.AuthPhoneRepository;
import com.example.kickboardapi.repository.MemberRepository;
import com.example.kickboardapi.service.message.MessageService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MemberDataService {
    private final MemberRepository memberRepository;
    private final PasswordEncoder passwordEncoder;
    private final AuthPhoneRepository authPhoneRepository;
    private final MessageService messageService;

    public void putPassword(FindPasswordRequest request) {
        // 원본 가져오기..
        Optional<Member> member = memberRepository.findById(1L);

        // 만약... 데이터가 없으면... 던져야함...
        if (member.isEmpty()) throw new CMissingDataException();

        String newPassword = makeRandomPassword();

        Member originData = member.get();
        originData.putPassword(passwordEncoder.encode(newPassword));
        memberRepository.save(originData);
        // --- 여기까지 비밀번호 변경 완료.. 비번 변경 완료 했으면 문자 보내야함...
        // 문자 보내는거... 이거 컨트롤러에서 할거 아님...
        // 서비스를 도와주는 서비스... 문자(카톡)전송 서비스 얘한테 부탁할거임..

        HashMap<String, String> msgMap = new HashMap<>();
        msgMap.put("임시비번", newPassword);

        messageService.sendMessage(
                MessageTemplate.TD_6785,
                "1577-1577",
                "010-0000-0000",
                msgMap
        );

        // -- 여기까지가 문자 전송..끝~~
    }

    private String makeRandomPassword() {
        return "12345678";
    }

    public void putPassword(Member member, PasswordChangeRequest changeRequest) {
        // 현재 비밀번호가 맞지 않으면 던져야 함.
        if (!passwordEncoder.matches(changeRequest.getCurrentPassword(), member.getPassword())) throw new CMissingDataException();

        // 변경할 비밀번호와 변경 비밀번호 확인 값이 일치하지 않으면 던져야함.
        if (!changeRequest.getChangePassword().equals(changeRequest.getChangePasswordRe())) throw new CWrongPhoneNumberException();

        member.putPassword(passwordEncoder.encode(changeRequest.getChangePassword()));
        memberRepository.save(member);
    }

    public void setFirstMember() {
        String username = "superadmin";
        String password = "idjh195233";
        boolean isSuperAdmin = isNewUsername(username);

        if (isSuperAdmin) {
            MemberCreateRequest createRequest = new MemberCreateRequest();
            createRequest.setUsername(username);
            createRequest.setPassword(password);
            createRequest.setPasswordRe(password);
            createRequest.setName("최고관리자");

            setMember(MemberGroup.ROLE_ADMIN, createRequest);
        }
    }

    public void setMember(MemberGroup memberGroup, MemberCreateRequest createRequest) {
        if (!CommonCheck.checkUsername(createRequest.getUsername())) throw new CWrongPhoneNumberException(); // 유효한 아이디 형식이 아닙니다 던지기
        if (!createRequest.getPassword().equals(createRequest.getPasswordRe())) throw new CWrongPhoneNumberException(); // 비밀번호가 일치하지 않습니다 던지기
        if (!isNewUsername(createRequest.getUsername())) throw new CWrongPhoneNumberException(); // 중복된 아이디가 존재합니다 던지기

        createRequest.setPassword(passwordEncoder.encode(createRequest.getPassword()));

        Member member = new Member.MemberBuilder(memberGroup, createRequest).build();
        memberRepository.save(member);
    }

    public void setMember(MemberCreateRequest createRequest) {
        // 진짜 본인인증 했냐??

        // 핸드폰번호 기준으로 인증한 데이터 원본 가져오기.
        Optional<AuthPhone> authPhone = authPhoneRepository.findByPhoneNumber(createRequest.getPhoneNumber());

        // 먄약.. 인증데이터가 없으면.. 인증시도도 안한거... 그러면 가입 시키면 안됨.. 그니까 던져야함..
        if (authPhone.isEmpty()) throw new CMissingDataException();

        // 만약.. 인증상태가 인증진행중이라면.. 가입시키면 안됨.. 그니까 던져야함...
        if (!authPhone.get().getIsAuthComplete()) throw new CMissingDataException();

        setMember(MemberGroup.ROLE_USER, createRequest);

        // 인증정보 다 썼으니 삭제해버리기...
        authPhoneRepository.deleteById(authPhone.get().getId());
    }

    private boolean isNewUsername(String username) {
        long dupCount = memberRepository.countByUsername(username);
        return dupCount <= 0;
    }
}
