package com.example.kickboardapi.service.kickBoard;

import com.example.kickboardapi.entity.KickBoard;
import com.example.kickboardapi.enums.KickBoardStatus;
import com.example.kickboardapi.enums.PriceBasis;
import com.example.kickboardapi.lib.CommonFile;
import com.example.kickboardapi.model.common.ListResult;
import com.example.kickboardapi.model.kickboard.KickBoardItem;
import com.example.kickboardapi.model.kickboard.KickBoardRequest;
import com.example.kickboardapi.model.kickboard.KickBoardSearchRequest;
import com.example.kickboardapi.repository.KickBoardRepository;
import com.example.kickboardapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class KickBoardService {
    private final KickBoardRepository kickBoardRepository;

    @PersistenceContext
    EntityManager entityManager;

    public void setKickBoards(MultipartFile csvFile) throws IOException {
        File file = CommonFile.multipartToFile(csvFile);
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

        String line = "";
        int index = 0; // 줄 번호
        while ((line = bufferedReader.readLine()) != null) {
            if (index > 0) {
                String[] cols = line.split(",");

                // 킥보드 등록하려면 빌더 호출해야하는데 빌더에서 리퀘스트를 달래.. 그래서 리쉐스트를 준비한다.
                KickBoardRequest kickBoardRequest = new KickBoardRequest();
                kickBoardRequest.setModelName(cols[0]);
                kickBoardRequest.setPriceBasis(PriceBasis.valueOf(cols[1]));
                kickBoardRequest.setPosX(Double.parseDouble(cols[2]));
                kickBoardRequest.setPosY(Double.parseDouble(cols[3]));
                kickBoardRequest.setDateBuy(LocalDate.now());

                KickBoard kickBoard = new KickBoard.Builder(kickBoardRequest).build(); // 준비한 리퀴스트 주면서 킥보드로 바꿔오고
                kickBoardRepository.save(kickBoard); // 저장
            }
            index++; // index = index + 1;
        }
        bufferedReader.close();
    }

    public ListResult<KickBoardItem> getKickboards(int pageNum, KickBoardSearchRequest searchRequest) {
        PageRequest pageRequest = ListConvertService.getPageable(pageNum, 10);

        Page<KickBoard> kickBoards = getKickboards(pageRequest, searchRequest);

        ListResult<KickBoardItem> result = new ListResult<>();
        result.setTotalItemCount(kickBoards.getTotalElements());
        result.setTotalPage(kickBoards.getTotalPages() == 0 ? 1 : kickBoards.getTotalPages());
        result.setCurrentPage(kickBoards.getPageable().getPageNumber() + 1);

        List<KickBoardItem> list = new LinkedList<>();
        for (KickBoard kickBoard : kickBoards.getContent()) {
            list.add(
                    new KickBoardItem.Builder(
                            kickBoard.getKickBoardStatus(),
                            kickBoard.getModelName(),
                            kickBoard.getPriceBasis(),
                            kickBoard.getPosX(),
                            kickBoard.getPosY(),
                            0
                    ).build());
        }
        result.setList(list);

        return result;
    }


    private Page<KickBoard> getKickboards(Pageable pageable, KickBoardSearchRequest searchRequest) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<KickBoard> criteriaQuery = criteriaBuilder.createQuery(KickBoard.class);

        Root<KickBoard> root = criteriaQuery.from(KickBoard.class);

        List<Predicate> predicates = new LinkedList<>();
        if (searchRequest.getKickBoardStatus() != null) predicates.add(criteriaBuilder.equal(root.get("kickBoardStatus"), searchRequest.getKickBoardStatus()));
        if (searchRequest.getModelName() != null) predicates.add(criteriaBuilder.like(root.get("modelName"), "%" + searchRequest.getModelName() + "%"));
        if (searchRequest.getPriceBasis() != null) predicates.add(criteriaBuilder.equal(root.get("priceBasis"), searchRequest.getPriceBasis()));
        if (searchRequest.getIsUse() != null) predicates.add(criteriaBuilder.equal(root.get("isUse"), searchRequest.getIsUse()));

        Predicate[] predArray = new Predicate[predicates.size()];
        predicates.toArray(predArray);
        criteriaQuery.where(predArray);

        TypedQuery<KickBoard> query = entityManager.createQuery(criteriaQuery);

        int totalRows = query.getResultList().size();

        query.setFirstResult(pageable.getPageNumber() * pageable.getPageSize());
        query.setMaxResults(pageable.getPageSize());

        return new PageImpl<>(query.getResultList(), pageable, totalRows);
    }

    public ListResult<KickBoardItem> getNearList(double posX, double posY, int distanceKm) {
        double distanceResult = distanceKm * 1000; // 사람들이 요청할때 근처 1키로미터 ~~ 이렇게 요청하면 이걸 미터 기준으로 변환.. x 1000

        String queryString = "select * from public.get_near_kickboard(" + posX + ", " + posY + ", " + distanceResult + ")"; // 쿼리 문자열(String)로 준비하기
        Query nativeQuery = entityManager.createNativeQuery(queryString); // entityManager(DB 담당하는애)한테 위에서 작성한 쿼리모양 문자열을 쿼리 객체로 바꿔주기
        List<Object[]> queryList = nativeQuery.getResultList(); // 쿼리 실행해서 리스트 가져오기.. 근데 얘는 모양이 확정된애가 아니라서 object(타입이 아무것도 정해지지않은 객체)로 가져오기

        List<KickBoardItem> result = new LinkedList<>(); // 결과값 담을 빈 리스트 준비
        for (Object[] queryItem : queryList) { // 쿼리 실행해서 가져온 리스트에서 한줄씩 던져주면서
            result.add( // 결과값 담을 리스트에 추가해준다.
                    new KickBoardItem.Builder( // KickBoardItem 모양으로 담아야 하니까 KickBoardItem 빌더 호출
                            KickBoardStatus.valueOf(queryItem[0].toString()), // KickBoardStatus Enum 값들중에서 queryItem의 0번째 요소를 문자열로 바꾼애랑 일치하는 값이 있다면 그 값으로 바꿔오기
                            queryItem[1].toString(), // queryItem의 1번째 요소를 문자열로 캐스팅
                            PriceBasis.valueOf(queryItem[2].toString()), // PriceBasis Enum 값들중에서 queryItem의 2번째 요소를 문자열로 바꾼애랑 일치하는 값이 있다면 그 값으로 바꿔오기
                            Double.parseDouble(queryItem[3].toString()), // queryItem의 3번째 요소를 오브젝트 -> 문자열 -> 더블로 2번 캐스팅
                            Double.parseDouble(queryItem[4].toString()), // queryItem의 4번째 요소를 오브젝트 -> 문자열 -> 더블로 2번 캐스팅
                            Double.parseDouble(queryItem[5].toString()) // queryItem의 5번째 요소를 오브젝트 -> 문자열 -> 더블로 2번 캐스팅
                    ).build()
            );
        }

        return ListConvertService.settingResult(result); // 결과값이 다 완성되었으므로 돌려주기
    }
}
